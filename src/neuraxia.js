module.exports = {

  "Neurone": require("./classes/Neurone"),
  "Layer": require("./classes/Layer"),
  "nn": require("./activations"),
  "losses": require("./losses"),
  "aux": require("./util/auxils")

};

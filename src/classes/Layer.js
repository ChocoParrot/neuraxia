var Neurone = require("./Neurone");
var activations = require("../activations");

module.exports = class {

  constructor () {

    this.units = new Array();

  }

  forward (values) {

    var outputs = new Array();

    if (values !== undefined) {
      for (var i = 0; i < this.units.length; i++) {
        if (this.units[i].forward !== undefined) {
          outputs.push(this.units[i].forward(values[i]));
        };
      };
    } else {
      for (var i = 0; i < this.units.length; i++) {
        if (this.units[i].forward !== undefined) {
          outputs.push(this.units[i].forward());
        };
      };
    };

    return outputs;

  }

  createNeurones (units, activation=activations.tanh, bias=true) {

    for (var i = 0; i < units; i++) {
      this.units.push(new Neurone(activation, bias));
    };

    return this;

  }

  connect (to) {

    if (to instanceof module.exports) {

      // TODO: Make _neurone() method to create simple
      // iterations;

      for (var i = 0; i < to.units.length; i++) {
        for (var j = 0; j < this.units.length; j++) {
          this.units[j].connect(to.units[i]);
        };
      };
    };

    return to;

  }

  add (units) {

    this.units = this.units.append(units);

    return this;

  }

  computeGradients (derivatives) {
    var gradients = new Array();

    if (derivatives !== undefined) {
      for (var i = 0; i < this.units.length; i++) {
        gradients.push(this.units[i].computeGradients(derivatives[i]));
      };
    } else {
      for (var i = 0; i < this.units.length; i++) {
        gradients.push(this.units[i].computeGradients());
      };
    };

    //console.log(gradients);

    return gradients;

  }

  applyGradients (gradients) {
    if (gradients !== undefined) {
      for (var i = 0; i < this.units.length; i++) {
        this.units[i].applyGradients(gradients[i]);
      };
    } else {
      for (var i = 0; i < this.units.length; i++) {
        this.units[i].applyGradients();
      };
    };
  }

  _backpropagate (gradients) {
    if (gradients !== undefined) {
      for (var i = 0; i < this.units.length; i++) {
        this.units[i].backpropagate(gradients[i]);
      };
    } else {
      for (var i = 0; i < this.units.length; i++) {
        this.units[i].backpropagate();
      };
    };
  }

}

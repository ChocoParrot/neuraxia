var Connection = require("./Connection");
var activations = require("../activations");

module.exports = class {

  constructor (activation=activations.identity, bias=true) {

    this.backconnections = new Array();
    this.value = undefined;
    // TODO: Add BPTT functionality, state removal

    this.bias = 0;

    this.chain_derivative = 0;
    this.gradients = 0;

    this.activation = activation;

    if (bias) {
      this.bias = Math.random() * 2 - 1;
    };

  }

  forward (value) {

    if (value === undefined) {
      // Addition; compute sigma
      var value = 0;
      for (var i = 0; i < this.backconnections.length; i++) {

        /* Sigma (summation operation)
        ∑(w * x + b)*/

        value += this.backconnections[i].weight * this.backconnections[i].from.value;

      };

    };

    this.chain_derivative = 0;

    this.value = this.activation.forward(value + this.bias);

    return this.value;

  }

  connect (neurone) {

    var connection = new Connection(this, neurone);

    neurone.backconnections.push(connection);

    return neurone;

  }

  computeGradients (derivative) {
    /* Automatically use chain derivative (assume MLP)
    if derivative is not provided*/

    /* Gradients are compuuted wrt to the sum
    derivative (sigma derivative), not weights;
    capping of gradients is hence limited. */

    if (derivative === undefined) {
      derivative = this.chain_derivative;
    };

    // Using chain derivative for backpropagation
    var gradients = this.activation.backward(derivative) * derivative;

    for (var i = 0; i < this.backconnections.length; i++) {
      /* This operation updates the chain derivatives upstream.
      The process is automatic; this ensures backpropagation is proper
      regardless of whether or not neurones are hindered. */
      this.backconnections[i].updateChainDerivative(gradients);
    };

    this.gradients += gradients;

    return gradients;
  }

  applyGradients (gradients) {
    if (gradients === undefined) {
      gradients = this.gradients;
    };

    for (var i = 0; i < this.backconnections.length; i++) {

      // This operation updates the weights
      this.backconnections[i].updateWeight(gradients);

    };

  }

  backpropagate (derivative) {

    /* Convenience function for low-level
    API usage. */

    var gradients = this.computeGradients(derivative);
    //console.log(gradients);
    this.applyGradients(gradients);

  }

  _clearGradients () {

    this.gradients = 0;

  }

};

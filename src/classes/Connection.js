module.exports = class {

  constructor (from, to) {

    this.from = from;
    this.to = to;

    this.weight = Math.random();

  }

  updateChainDerivative (derivative) {
    this.from.chain_derivative += this.weight * derivative;
    return this;
  }

  updateWeight (derivative) {
    this.weight -= this.from.value * derivative;
    return this;
  }

};

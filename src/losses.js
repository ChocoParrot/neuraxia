module.exports = {

  "mse": {forward: (y, x) => 1/2 * Math.pow(y - x, 2), backward: (y, x) => x - y},
  "logcosh": {forward: (y, x) => Math.log(Math.cosh(y - x)), backward: (y, x) => Math.tanh(y - x) * -x}

};

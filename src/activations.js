module.exports = {

  "identity": {"forward": x => x, "backward": x => 1},
  "tanh": {"forward": x => Math.tanh(x), "backward": x => (1 - Math.pow(Math.tanh(x), 2))},
  "sin": {"forward": x => Math.sin(x), "backward": x => Math.cos(x)},
  "cos": {"forward": x => Math.cos(x), "backward": x => -Math.sin(x)},
  "relu": {"forward": x => Math.max(x, 0), "backward": x => x > 0 ? 1 : 0},
  "leaky_relu": {"forward": x => x > 0 ? x : 0.1 * x, "backward": x => x > 0 ? 1 : 0.1}

};

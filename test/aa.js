var nx = require("../src/neuraxia");

var neurone = new nx.Neurone(nx.nn.tanh);
var neurone2 = new nx.Neurone(nx.nn.tanh);
var neurone25 = new nx.Neurone(nx.nn.tanh);
var neurone3 = new nx.Neurone(nx.nn.identity);

neurone.connect(neurone2);
neurone.connect(neurone25);
neurone25.connect(neurone3);
neurone2.connect(neurone3);

var expected = 0.712;

for (var i = 0; i < 500; i++) {
  neurone.forward(6);
  neurone2.forward();
  neurone25.forward();
  var output = neurone3.forward();

  var loss = 1/2 * Math.pow(expected - output, 2);
  var derivative = nx.losses.mse.backward(expected, output);
  neurone3.backpropagate(derivative);
  neurone25.backpropagate();
  neurone2.backpropagate();
  neurone.backpropagate();

  console.log(output);
};

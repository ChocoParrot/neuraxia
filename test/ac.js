var nx = require("../src/neuraxia");

A = new nx.Layer().createNeurones(1, activation=nx.nn.tanh);
B = new nx.Layer().createNeurones(16, activation=nx.nn.tanh);
C = new nx.Layer().createNeurones(16, activation=nx.nn.tanh);
D = new nx.Layer().createNeurones(16, activation=nx.nn.tanh);
E = new nx.Layer().createNeurones(1, activation=nx.nn.tanh);

//var start = new Date();

A.connect(B).connect(C).connect(D).connect(E);

for (var i = 0; i < 100000; i++) {

  var point = (Math.random() * 2 - 1) * Math.PI;

  A.forward([point]);
  B.forward();
  C.forward();
  D.forward();
  var output = E.forward();
  var expected = Math.sin(point);

  var derivative = nx.losses.mse.backward(expected, output) * 0.0001;

  E._backpropagate([derivative])
  D._backpropagate();
  C._backpropagate();
  B._backpropagate();
  A._backpropagate();

  //console.log(D.units[0].backconnections[0].weight);

  //console.log(D.units[0]);
  console.log("Real: %s, Out: %s", expected, output);

};

//var end = new Date();

//console.log((end.getTime() - start.getTime()) / 1000);

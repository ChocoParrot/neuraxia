var nx = require("../src/neuraxia");

A = new nx.Layer().createNeurones(1, activation=nx.nn.tanh);
B = new nx.Layer().createNeurones(5, activation=nx.nn.tanh);
C = new nx.Layer().createNeurones(5, activation=nx.nn.tanh);
D = new nx.Layer().createNeurones(1, activation=nx.nn.tanh);

A.connect(B).connect(C).connect(D);

for (var i = 0; i < 1000; i++) {
  A.forward([5]);
  B.forward();
  C.forward();
  var output = D.forward();

  var derivative = nx.losses.mse.backward(-0.4, output) * 0.1;

  D._backpropagate([derivative]);
  C._backpropagate();
  B._backpropagate();
  A._backpropagate();

  //console.log(D.units[0].backconnections[0].weight);
  console.log(output);
};

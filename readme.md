## Neuraxia ##
A middle-level research-oriented Neural Network library.
Uses an eager computation graph.

Work in progress.

Neuraxia is based off Neuras, a private and completed version of this library developed in late 2017.

## Details ##
Author: @ChocoParrot

Contributor(s): Robbe Pincket (@Kroppeb)
